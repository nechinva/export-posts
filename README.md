# Project Title

A plugin that allows you to export various posts to one of the selected format (html, pdf, doc, txt) using various settings

### Installing

Activate the plugin and go to the Tools menu section

## Built With

* [WordPress](https://wordpress.org/) - WordPress

## Versioning

0.7.0
* Updated phpword and dompdf libraries

0.6.0
* Added filter by category.
* Moved the export functionality to the Tools section.
* Removed settings page.
* Added function to save settings during export.
* Empty options are now hiding.
* Done some improvements.
* Fixed some bugs.

0.5.1
* Fixed some bugs.

0.5.0
* Added filters by start date and end date.
* Fixed some bugs.
* Done some improvements.

0.4.2
* Added field min date.
* Done some improvements.

0.4.1
* Added loader for post count.
* Add make temp folder.
* Done some improvements.

0.4.0
* Added strip tags option.
* Added strip shortcode option.
* Added get instance function.
* Delete unused code and files.
* Done some inprovements.

0.3.2
* Added changing post count values when changed post type.
* Done some improvements.

0.3.1
* Added dynamic split for post count by type.
* Done some improvements.

0.3.0
* Added localization.
* Added field for select dividing string for divide posts in export file.
* Done some improvements.

0.2.2
* Added field for naming export file.
* Improved working with images for pdf and doc export.

0.2.1
* Change code for strip tags.
* Rename libs directory.

0.2.0
* Fixed doc export type.
* Make code refactoring.

0.1.0
* Added doc export type.
* Done some improvements.

0.0.4
* Added pdf export type.
* Done some improvements.

0.0.3
* Added export type selector.
* Added html export type.

0.0.2
* Added export and settings page.

0.0.1
* Start.

## Authors

* **Alexander Vitkalov** - *All work* - [vitkalov.ru](http://vitkalov.ru)

## License

GPL2