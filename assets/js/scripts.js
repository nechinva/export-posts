jQuery(document).ready(function($) {
	// Activate/deactivate codes with AJAX
	$('select#smart_eps_type').change(function(e) {
		var type = $(this).val();
		var el_counts = $('select#smart_eps_count'),
			el_category = $('select#smart_eps_category'),
			el_start_date = $('select#smart_eps_start_date'),
			el_end_date = $('select#smart_eps_end_date');

		el_counts.prop('disabled', true);
		el_category.prop('disabled', true);
		el_start_date.prop('disabled', true);
		el_end_date.prop('disabled', true);

		$('img.smart-eps-loader').show();

		$.post(
			ajaxurl,
			{
				action: 'smart_eps_get_html_count_for_type',
				type: type
			},
			function(data) {
				if (data.counts !== '') {
					el_counts.parent('td').parent('tr').show();
					el_counts.html(data.counts);
					el_counts.prop('disabled', false);
				} else {
					el_counts.parent('td').parent('tr').hide();
				}

				if (data.categories !== '') {
					el_category.parent('td').parent('tr').show();
					el_category.html(data.categories);
					el_category.prop('disabled', false);
				} else {
					el_category.parent('td').parent('tr').hide();
				}

				if (data.start_date !== '') {
					el_start_date.html(data.start_date);
				}
				el_start_date.prop('disabled', false);

				if (data.end_date !== '') {
					el_end_date.html(data.end_date);
				}
				el_end_date.prop('disabled', false);

				$('img.smart-eps-loader').hide();
			}, "json"
		);
	});

	if ($('select#smart_eps_type').val() !== 'post') {
		$('select#smart_eps_category').parent('td').parent('tr').hide();
	}

	if ($('#smart-eps-export-count').val() == 0) {
		$('select#smart_eps_count').parent('td').parent('tr').hide();
	}
});
