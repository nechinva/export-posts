<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Register ajax requests and execution actions
 *
 * @author Alexander Vitkalov <nechin.va@gmail.com>
 * @copyright (c) 29.04.2019, Vitkalov
 * @version 1.0
 */
class Smart_Eps_Ajax {

	/**
	 * Smart_Eps_Ajax constructor.
	 */
	public function __construct() {
		if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
			add_action( 'wp_ajax_smart_eps_get_html_count_for_type', [ $this, 'get_html_count_for_type' ] );
		}
	}

	/**
	 * Get html code for select element
	 */
	public function get_html_count_for_type() {
		$type = isset( $_POST['type'] ) ? $_POST['type'] : '';

		$response = [
			'counts'     => '',
			'categories' => '',
			'start_date' => '',
			'end_date'   => '',
		];

		if ( $type ) {
			$counts = smart_eps_instance()->job->get_counts( $type );
			if ( $counts ) {
				$response['counts'] .= '<option value="-1">' . __( 'All', 'smart-eps' ) . '</option>';
				foreach ( $counts as $count ) {
					$response['counts'] .= '<option value="' . $count . '">' . $count . '</option>';
				}
			}

			$categories = smart_eps_instance()->job->get_categories( $type );
			if ( $categories ) {
				$response['categories'] .= '<option value="0">' . __( 'All', 'smart-eps' ) . '</option>';
				foreach ( $categories as $category_id => $category_name ) {
					$response['categories'] .= '<option value="' . $category_id . '">' . $category_name . '</option>';
				}
			}

			$response['start_date'] .= '<option value="">' . __( 'All', 'smart-eps' ) . '</option>';
			$response['end_date']   .= '<option value="">' . __( 'All', 'smart-eps' ) . '</option>';
			$dates   = smart_eps_instance()->job->get_dates_interval( $type );
			if ( $dates ) {
				foreach ( $dates as $date ) {
					$response['start_date'] .= '<option value="' . $date['value'] . '">' . $date['title'] . '</option>';
				}
				foreach ( $dates as $date ) {
					$response['end_date'] .= '<option value="' . $date['value'] . '">' . $date['title'] . '</option>';
				}
			}
		}

		wp_send_json( $response );
	}

}
