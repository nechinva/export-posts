<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Base class
 *
 * @author Alexander Vitkalov <nechin.va@gmail.com>
 * @copyright (c) 29.04.2019, Vitkalov
 * @version 1.0
 */
class Smart_Eps_Base {

	/**
	 * @var null
	 */
	protected static $_instance = null;

	/**
	 * @var null
	 */
	public $hook = null;

	/**
	 * @var null
	 */
	public $job = null;

	/**
	 * @var null
	 */
	public $settings = null;

	/**
	 * Smart_Eps_Base constructor.
	 */
	public function __construct() {
		$this->includes();

		$this->hook     = new Smart_Eps_Hook();
		$this->job      = new Smart_Eps_Job();
		$this->settings = new Smart_Eps_Settings();
		new Smart_Eps_Ajax();

		$this->bind_hooks();
	}

	/**
	 * Get instatce
	 *
	 * @return Smart_Eps_Base|null
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Include files
	 */
	private function includes() {
		require_once SMART_EPS_DIR_INCLUDES . 'class-smart-eps-hook.php';
		require_once SMART_EPS_DIR_INCLUDES . 'class-smart-eps-job.php';
		require_once SMART_EPS_DIR_INCLUDES . 'class-smart-eps-settings.php';
		require_once SMART_EPS_DIR_INCLUDES . 'class-smart-eps-ajax.php';
	}

	/**
	 * Bind hooks
	 */
	private function bind_hooks() {
		register_activation_hook( SMART_EPS_PATH, [ $this->hook, 'activation' ] );
		register_uninstall_hook( SMART_EPS_PATH, [ 'Smart_Eps_Hook', 'uninstall' ] );

		// add_action( 'init', [ $this->hook, 'wp_init' ] );
		add_action( 'admin_init', [ $this->hook, 'load_plugin_textdomain' ] );
		add_action( 'admin_enqueue_scripts', [ $this->hook, 'admin_enqueue_scripts' ] );

		add_action( 'admin_init', [ $this->job, 'wp_init' ] );
		add_action( 'admin_menu', [ $this->job, 'create_menu' ] );

		add_action( 'admin_init', [ $this->settings, 'create_settings' ] );
	}

	/**
	 * Run execution
	 */
	public function run() {
		// Start
	}

}
