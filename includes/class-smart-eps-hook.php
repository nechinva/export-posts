<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Hooks class
 *
 * @author Alexander Vitkalov <nechin.va@gmail.com>
 * @copyright (c) 29.04.2019, Vitkalov
 * @version 1.0
 */
class Smart_Eps_Hook {

	/**
	 * Smart_Eps_Hook constructor.
	 */
	public function __construct() {
	}

	/**
	 * Activation plugin
	 */
	public function activation() {
	}

	/**
	 * Uninstall plugin
	 */
	public function uninstall() {
		unregister_setting( 'eps', 'eps_options' );
	}

	/**
	 * Load plugin textdomain
	 */
	public function load_plugin_textdomain() {
		load_plugin_textdomain( 'smart-eps', false, SMART_EPS_PLUGIN_BASE . '/languages' );
	}

	/**
	 * Enqueue scripts
	 */
	public function admin_enqueue_scripts() {
		wp_enqueue_script( 'smart-eps-script', SMART_EPS_URL . '/assets/js/scripts.js' );
		wp_enqueue_style( 'smart-eps-style', SMART_EPS_URL . '/assets/css/style.css' );
	}

}
