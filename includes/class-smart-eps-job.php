<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Jobs class
 *
 * @author Alexander Vitkalov <nechin.va@gmail.com>
 * @copyright (c) 29.04.2019, Vitkalov
 * @version 1.0
 */
class Smart_Eps_Job {

	/**
	 * Smart_Eps_Job constructor.
	 */
	public function __construct() {
	}

	/**
	 * Create new menu
	 */
	public function create_menu() {
		add_management_page(
			__( 'Smart Export Posts', 'smart-eps' ),
			__( 'Smart Export Posts', 'smart-eps' ),
			'administrator',
			SMART_EPS_SLUG,
			array(
				$this,
				'show_export',
			)
        );
	}

	/**
	 * Show export posts page
	 */
	public function show_export() {
		// Check if the user have submitted the export
		// WordPress will add the "settings-updated" $_GET parameter to the url
		if ( isset( $_POST['settings-error'] ) ) {
			// Add settings saved message with the class of "updated"
			add_settings_error( 'smart_eps_messages', 'smart_eps_messages', __( 'Export Error', 'smart-eps' ) );
		}

		// Show error/update messages
		settings_errors( 'smart_eps_messages' );
		?>
		<div class="wrap">
			<h1><?php _e( 'Smart Export Posts', 'smart-eps' ); ?></h1>
			<form action="" method="post">
				<input type="hidden" name="<?php echo SMART_EPS_OPTION_GROUP ?>-do-export" value="1">
				<?php
				wp_nonce_field( SMART_EPS_OPTION_GROUP . '-options', SMART_EPS_OPTION_GROUP . '-export' );
				// output setting sections and their fields
				do_settings_sections( SMART_EPS_SLUG );
				// output save settings button
				submit_button( __( 'Export', 'smart-eps' ) );
				?>
			</form>
		</div>
		<?php
	}

	/**
	 * Check if type is available
	 *
	 * @param $type string
	 *
	 * @return bool
	 */
	public function check_available_type( $type ) {
		foreach ( get_post_types( [ 'public' => true ], 'objects' ) as $post_type ) {
			if ( $type == $post_type->name ) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Везвращает массив разбивки количества записей указанного типа
	 *
	 * @param $type string
	 *
	 * @return array
	 */
	public function get_counts( $type ) {
		if ( ! $this->check_available_type( $type ) ) {
			return [];
		}

		$posts = get_posts(
			[
				'post_type'      => $type,
				'posts_per_page' => - 1,
			]
		);

		$a = [];

		if ( count( $posts ) < 6 ) {
			return $a;
		} elseif ( count( $posts ) < 20 ) {
			for ( $i = 5; $i <= count( $posts ); $i += 5 ) {
				$a[] = $i;
			}
		} elseif ( count( $posts ) < 50 ) {
			for ( $i = 10; $i <= count( $posts ); $i += 10 ) {
				$a[] = $i;
			}
		} elseif ( count( $posts ) < 100 ) {
			for ( $i = 20; $i <= count( $posts ); $i += 20 ) {
				$a[] = $i;
			}
		} elseif ( count( $posts ) < 500 ) {
			for ( $i = 100; $i <= count( $posts ); $i += 100 ) {
				$a[] = $i;
			}
		} elseif ( count( $posts ) < 1000 ) {
			for ( $i = 200; $i <= count( $posts ); $i += 200 ) {
				$a[] = $i;
			}
		} else {
			return [ 100, 1000, 2500, 5000 ];
		}

		return $a;
	}

	/**
     * Возвращает список рубрик для типа записи post
     *
	 * @param $type
	 *
	 * @return array
	 */
	public function get_categories( $type ) {
		$categories = [];

		if ( ! $this->check_available_type( $type ) ) {
			return $categories;
		}

		if ( 'post' === $type ) {
			$args = [
				'taxonomy' => 'category',
				'orderby'  => 'name',
				'order'    => 'ASC'
			];

			$cats = get_categories( $args );

			foreach ( $cats as $cat ) {
				$categories[ $cat->term_id ] = $cat->name;
			}
		}

		return $categories;
	}

	/**
     * Возвращает список дат вида (2018-07 => Июль 2018) от минимальной даты указанной записи, до максимальной
     *
	 * @param $type string
	 *
	 * @return array
	 */
	public function get_dates_interval( $type ) {
		if ( ! $this->check_available_type( $type ) ) {
			return [];
		}

		$interval = [];

		global $wpdb, $wp_locale;

		$months = $wpdb->get_results(
			$wpdb->prepare(
				"
                    SELECT DISTINCT YEAR( post_date ) AS year, MONTH( post_date ) AS month
                    FROM $wpdb->posts
                    WHERE post_type = %s AND post_status != 'auto-draft'
                    ORDER BY post_date DESC
                ",
				$type
			)
		);

		$month_count = count( $months );
		if ( ! $month_count || ( 1 == $month_count && 0 == $months[0]->month ) ) {
			return [];
		}

		foreach ( $months as $date ) {
			if ( 0 == $date->year ) {
				continue;
			}

			$month = zeroise( $date->month, 2 );
			$interval[] = [
			    'value' => $date->year . '-' . $month,
			    'title' => $wp_locale->get_month( $month ) . ' ' . $date->year,
            ];
		}

		return $interval;
	}

	/**
	 * Save options
	 */
	private function save_options() {
		$options = $_POST[ SMART_EPS_OPTIONS ];
		update_option( SMART_EPS_OPTIONS, $options );
	}

	/**
	 * Export posts
	 */
	public function wp_init() {
		if (
			! empty( $_POST[ SMART_EPS_OPTION_GROUP . '-do-export' ] )
			&& wp_verify_nonce( $_POST[ SMART_EPS_OPTION_GROUP . '-export' ], SMART_EPS_OPTION_GROUP . '-options' )
		) {
		    $this->save_options();

			$format = isset( $_POST[ SMART_EPS_OPTIONS ][ SMART_EPS_SLUG . '_export_format' ] )
				? $_POST[ SMART_EPS_OPTIONS ][ SMART_EPS_SLUG . '_export_format' ]
				: 'text';
			$name   = isset( $_POST[ SMART_EPS_OPTIONS ][ SMART_EPS_SLUG . '_name' ] )
				? $_POST[ SMART_EPS_OPTIONS ][ SMART_EPS_SLUG . '_name' ]
				: 'Export {TYPE}';
			$name   = str_replace( '{TYPE}', $format, $name );

			if ( file_exists( SMART_EPS_DIR_TYPES . "class-export-$format.php" ) ) {
				require_once SMART_EPS_DIR_TYPES . 'class-export-base.php';
				require_once SMART_EPS_DIR_TYPES . "class-export-$format.php";
				$class_name = 'Export_' . ucfirst( $format );
				$content    = ( new $class_name( $name ) )->export();

				echo $content;
				exit;
			}
		}
	}
}
