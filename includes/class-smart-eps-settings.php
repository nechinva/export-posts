<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Settings page and options
 *
 * @author Alexander Vitkalov <nechin.va@gmail.com>
 * @copyright (c) 29.04.2019, Vitkalov
 * @version 1.0
 */
class Smart_Eps_Settings {

	/**
	 * Smart_Eps_Settings constructor.
	 */
	public function __construct() {
	}

	/**
	 * Return options with defaults
	 *
	 * @return mixed
	 */
	private function get_eps_options() {
		$default = [
			SMART_EPS_SLUG . '_name'            => 'Export',
			SMART_EPS_SLUG . '_export_format'   => 'txt',
			SMART_EPS_SLUG . '_type'            => 'post',
			SMART_EPS_SLUG . '_count'           => - 1,
			SMART_EPS_SLUG . '_category'        => '',
			SMART_EPS_SLUG . '_start_date'      => '',
			SMART_EPS_SLUG . '_end_date'        => '',
			SMART_EPS_SLUG . '_posts_divider'   => 0,
			SMART_EPS_SLUG . '_strip_tags'      => '',
			SMART_EPS_SLUG . '_strip_shotcodes' => '',
		];

		return get_option( SMART_EPS_OPTIONS, $default );
	}

	/**
	 * Create new settings group
	 */
	public function create_settings() {
		register_setting( SMART_EPS_OPTION_GROUP, SMART_EPS_OPTIONS );

		// Register a new section
		add_settings_section(
			'smart_eps_section_posts',
			__( 'Export settings', 'smart-eps' ),
			array(
				$this,
				'smart_eps_section_posts_cp',
			),
			SMART_EPS_SLUG
		);

		// Name for export file (template)
		add_settings_field(
			'smart_eps_name',
			__( 'File name', 'smart-eps' ),
			array(
				$this,
				'field_name_s',
			),
			SMART_EPS_SLUG,
			'smart_eps_section_posts',
			[
				'label_for' => 'smart_eps_name',
				//'class'     => 'eps_row',
				//'smart_eps_custom_data' => 'custom',
			]
		);

		// Export format
		add_settings_field(
			'smart_eps_export_format',
			__( 'Export format', 'smart-eps' ),
			array(
				$this,
				'export_format_posts_s',
			),
			SMART_EPS_SLUG,
			'smart_eps_section_posts',
			[
				'label_for' => 'smart_eps_export_format',
			]
		);

		// Posts type
		add_settings_field(
			'smart_eps_type',
			__( 'Posts type', 'smart-eps' ),
			array(
				$this,
				'field_type_posts_s',
			),
			SMART_EPS_SLUG,
			'smart_eps_section_posts',
			[
				'label_for' => 'smart_eps_type',
			]
		);

		// Posts count per export
		add_settings_field(
			'smart_eps_count',
			__( 'Posts count', 'smart-eps' ),
			array(
				$this,
				'field_count_posts_s',
			),
			SMART_EPS_SLUG,
			'smart_eps_section_posts',
			[
				'label_for' => 'smart_eps_count',
			]
		);

		// Posts count per export
		add_settings_field(
			'smart_eps_category',
			__( 'Category', 'smart-eps' ),
			array(
				$this,
				'field_category_posts_s',
			),
			SMART_EPS_SLUG,
			'smart_eps_section_posts',
			[
				'label_for' => 'smart_eps_category',
			]
		);

		// Posts start date
		add_settings_field(
			'smart_eps_start_date',
			__( 'Start date', 'smart-eps' ),
			array(
				$this,
				'field_start_date_posts_s',
			),
			SMART_EPS_SLUG,
			'smart_eps_section_posts',
			[
				'label_for' => 'smart_eps_start_date',
			]
		);

		// Posts end date
		add_settings_field(
			'smart_eps_end_date',
			__( 'End date', 'smart-eps' ),
			array(
				$this,
				'field_end_date_posts_s',
			),
			SMART_EPS_SLUG,
			'smart_eps_section_posts',
			[
				'label_for' => 'smart_eps_end_date',
			]
		);

		// Posts divider
		add_settings_field(
			'smart_eps_posts_divider',
			__( 'Posts divider', 'smart-eps' ),
			array(
				$this,
				'export_divider_posts_s',
			),
			SMART_EPS_SLUG,
			'smart_eps_section_posts',
			[
				'label_for' => 'smart_eps_posts_divider',
			]
		);

		// Strip tags
		add_settings_field(
			'smart_eps_strip_tags',
			__( 'Strip tags', 'smart-eps' ),
			array(
				$this,
				'export_strip_tags_posts_s',
			),
			SMART_EPS_SLUG,
			'smart_eps_section_posts',
			[
				'label_for' => 'smart_eps_strip_tags',
			]
		);

		// Strip shotcodes
		add_settings_field(
			'smart_eps_strip_shotcodes',
			__( 'Strip shotcodes', 'smart-eps' ),
			array(
				$this,
				'export_strip_shotcodes_posts_s',
			),
			SMART_EPS_SLUG,
			'smart_eps_section_posts',
			[
				'label_for' => 'smart_eps_strip_shotcodes',
			]
		);
	}

	/**
	 * Выводит поле ввода названия экспорта
	 *
	 * @param $args array
	 */
	public function field_name_s( $args ) {
		// get the value of the setting we've registered with register_setting()
		$options = $this->get_eps_options();
		// output the field
		?>
		<input id="<?php echo esc_attr( $args['label_for'] ); ?>" name="<?php echo SMART_EPS_OPTIONS; ?>[<?php echo esc_attr( $args['label_for'] ); ?>]" value="<?php echo $options[ $args['label_for'] ] ?>" type="text">
		<p class="description">
			<?php esc_html_e( 'Set template name for export file ({TYPE} - export format)', 'smart-eps' ); ?>
		</p>
		<?php
	}

	/**
	 * Выводит контент вверху секции настроек
	 *
	 * @param $args array
	 */
	public function smart_eps_section_posts_cp( $args ) {
		?>
		<p id="<?php echo esc_attr( $args['id'] ); ?>"><?php esc_html_e( 'Set export settings', 'smart-eps' ); ?></p>
		<?php
	}

	/**
	 * Выводит селектбокс выбора формата экспорта
	 *
	 * @param $args array
	 */
	public function export_format_posts_s( $args ) {
		// get the value of the setting we've registered with register_setting()
		$options = $this->get_eps_options();
		// output the field
		?>
        <select id="<?php echo esc_attr( $args['label_for'] ); ?>"
                name="<?php echo SMART_EPS_OPTIONS; ?>[<?php echo esc_attr( $args['label_for'] ); ?>]"
        >
            <option value="txt" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'txt', false ) ) : ( '' ); ?>><?php esc_html_e( 'Text', 'smart-eps' ); ?></option>
            <option value="html" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'html', false ) ) : ( '' ); ?>><?php esc_html_e( 'Html', 'smart-eps' ); ?></option>
            <option value="pdf" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'pdf', false ) ) : ( '' ); ?>><?php esc_html_e( 'Pdf', 'smart-eps' ); ?></option>
            <option value="docx" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'docx', false ) ) : ( '' ); ?>><?php esc_html_e( 'Doc', 'smart-eps' ); ?></option>
        </select>
        <p class="description">
			<?php esc_html_e( 'Set the format of the resulting file', 'smart-eps' ); ?>
        </p>
		<?php
	}

	/**
	 * Выводит селектбокс выбора типа записей для экспорта
	 *
	 * @param $args array
	 */
	public function field_type_posts_s( $args ) {
		// get the value of the setting we've registered with register_setting()
		$options = $this->get_eps_options();
		// output the field
		?>
		<select id="<?php echo esc_attr( $args['label_for'] ); ?>"
				name="<?php echo SMART_EPS_OPTIONS; ?>[<?php echo esc_attr( $args['label_for'] ); ?>]"
		>
			<?php foreach ( get_post_types( [ 'public' => true ], 'objects' ) as $post_type ) : ?>
				<option value="<?php echo $post_type->name; ?>" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], $post_type->name, false ) ) : ( '' ); ?>><?php echo $post_type->label; ?></option>
			<?php endforeach; ?>
		</select>
		<p class="description">
			<?php esc_html_e( 'Set posts type', 'smart-eps' ); ?>
		</p>
		<?php
	}

	/**
	 * Выводит селектбокс выбора количества записей для экспорта
	 *
	 * @param $args array
	 */
	public function field_count_posts_s( $args ) {
		global $smart_eps_base;
		// get the value of the setting we've registered with register_setting()
		$options = $this->get_eps_options();
		$counts  = $smart_eps_base->job->get_counts( $options[ SMART_EPS_SLUG . '_type' ] );
		// output the field
		?>
        <input type="hidden" id="<?php echo SMART_EPS_OPTION_GROUP ?>-export-count" value="<?php echo count($counts); ?>">
		<select id="<?php echo esc_attr( $args['label_for'] ); ?>"
				name="<?php echo SMART_EPS_OPTIONS; ?>[<?php echo esc_attr( $args['label_for'] ); ?>]"
		>
            <option value="-1" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], '100', false ) ) : ( '' ); ?>><?php esc_html_e( 'All', 'smart-eps' ); ?></option>
            <?php
		foreach ( $counts as $count ) {
			?>
			<option value="<?php echo $count; ?>" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], $count, false ) ) : ( '' ); ?>><?php echo $count; ?></option>
			<?php
		}
		?>
        </select><img src="<?php echo SMART_EPS_URL ?>/assets/images/loader.gif" alt="<?php esc_html_e( 'Loading...', 'smart-eps' ); ?>" class="smart-eps-loader"/>
		<p class="description">
			<?php esc_html_e( 'Set the count of posts', 'smart-eps' ); ?>
		</p>
		<?php
	}

	/**
	 * Выводит селектбокс выбора категории записей для экспорта
	 *
	 * @param $args array
	 */
	public function field_category_posts_s( $args ) {
		global $smart_eps_base;
		// get the value of the setting we've registered with register_setting()
		$options    = $this->get_eps_options();
		$categories = $smart_eps_base->job->get_categories( $options[ SMART_EPS_SLUG . '_type' ] );
		// output the field
		?>
		<select id="<?php echo esc_attr( $args['label_for'] ); ?>"
				name="<?php echo SMART_EPS_OPTIONS; ?>[<?php echo esc_attr( $args['label_for'] ); ?>]"
		>
            <option value="0" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], '', false ) ) : ( '' ); ?>><?php esc_html_e( 'All', 'smart-eps' ); ?></option>
            <?php
		foreach ( $categories as $category_id => $category_name ) {
			?>
			<option value="<?php echo $category_id; ?>" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], $category_id, false ) ) : ( '' ); ?>><?php echo $category_name; ?></option>
			<?php
		}
		?>
        </select><img src="<?php echo SMART_EPS_URL ?>/assets/images/loader.gif" alt="<?php esc_html_e( 'Loading...', 'smart-eps' ); ?>" class="smart-eps-loader"/>
		<p class="description">
			<?php esc_html_e( 'Set the category of posts', 'smart-eps' ); ?>
		</p>
		<?php
	}

	/**
	 * Выводит поле для указания начальной даты записей
	 *
	 * @param $args array
	 */
	public function field_start_date_posts_s( $args ) {
		$options = $this->get_eps_options();
		$dates   = smart_eps_instance()->job->get_dates_interval( $options[ SMART_EPS_SLUG . '_type' ] );
		// output the field
		?>
        <select id="<?php echo esc_attr( $args['label_for'] ); ?>"
                name="<?php echo SMART_EPS_OPTIONS; ?>[<?php echo esc_attr( $args['label_for'] ); ?>]"
        >
            <option value="" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], '', false ) ) : ( '' ); ?>><?php esc_html_e( 'All', 'smart-eps' ); ?></option>
	        <?php
	        foreach ( $dates as $date ) {
		        ?>
                <option value="<?php echo $date['value']; ?>" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], $date['value'], false ) ) : ( '' ); ?>><?php echo $date['title']; ?></option>
		        <?php
	        } ?>
        </select><img src="<?php echo SMART_EPS_URL ?>/assets/images/loader.gif" alt="<?php esc_html_e( 'Loading...', 'smart-eps' ); ?>" class="smart-eps-loader"/>
        <p class="description">
			<?php esc_html_e( 'Set a start date for posts', 'smart-eps' ); ?>
        </p>
		<?php
	}

	/**
	 * Выводит поле для указания конечной даты записей
	 *
	 * @param $args array
	 */
	public function field_end_date_posts_s( $args ) {
		$options = $this->get_eps_options();
		$dates   = smart_eps_instance()->job->get_dates_interval( $options[ SMART_EPS_SLUG . '_type' ] );
		// output the field
		?>
        <select id="<?php echo esc_attr( $args['label_for'] ); ?>"
                name="<?php echo SMART_EPS_OPTIONS; ?>[<?php echo esc_attr( $args['label_for'] ); ?>]"
        >
            <option value="" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], '', false ) ) : ( '' ); ?>><?php esc_html_e( 'All', 'smart-eps' ); ?></option>
	        <?php
	        foreach ( $dates as $date ) {
		        ?>
                <option value="<?php echo $date['value']; ?>" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], $date['value'], false ) ) : ( '' ); ?>><?php echo $date['title']; ?></option>
		        <?php
	        } ?>
        </select><img src="<?php echo SMART_EPS_URL ?>/assets/images/loader.gif" alt="<?php esc_html_e( 'Loading...', 'smart-eps' ); ?>" class="smart-eps-loader"/>
        <p class="description">
			<?php esc_html_e( 'Set an end date for posts', 'smart-eps' ); ?>
        </p>
		<?php
	}

	/**
	 * Выводит селектбокс выбора разделителя постов в файле
	 *
	 * @param $args array
	 */
	public function export_divider_posts_s( $args ) {
		// get the value of the setting we've registered with register_setting()
		$options = $this->get_eps_options();
		// output the field
		?>
		<select id="<?php echo esc_attr( $args['label_for'] ); ?>"
				name="<?php echo SMART_EPS_OPTIONS; ?>[<?php echo esc_attr( $args['label_for'] ); ?>]"
		>
			<option value="0" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], '0', false ) ) : ( '' ); ?>><?php esc_html_e( 'No divider', 'smart-eps' ); ?></option>
			<option value="1" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], '1', false ) ) : ( '' ); ?>><?php esc_html_e( 'New line', 'smart-eps' ); ?></option>
			<option value="2" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], '2', false ) ) : ( '' ); ?>><?php esc_html_e( 'Solid line (______)', 'smart-eps' ); ?></option>
			<option value="3" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], '3', false ) ) : ( '' ); ?>><?php esc_html_e( 'Dashed line (-----)', 'smart-eps' ); ?></option>
			<option value="4" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], '4', false ) ) : ( '' ); ?>><?php esc_html_e( 'Dotted line (.....)', 'smart-eps' ); ?></option>
		</select>
		<p class="description">
			<?php esc_html_e( 'Set divider for posts in export file', 'smart-eps' ); ?>
		</p>
		<?php
	}

	/**
	 * Выводит чекбокс, который позволяет убрать из содержимого постов медиафайлы и теги
	 *
	 * @param $args array
	 */
	public function export_strip_tags_posts_s( $args ) {
		// get the value of the setting we've registered with register_setting()
		$options = $this->get_eps_options();
		// output the field
		?>
		<input type="checkbox" id="<?php echo esc_attr( $args['label_for'] ); ?>" name="<?php echo SMART_EPS_OPTIONS; ?>[<?php echo esc_attr( $args['label_for'] ); ?>]" <?php echo isset( $options[ $args['label_for'] ] ) ? ( checked( $options[ $args['label_for'] ], '1', false ) ) : ( '' ); ?> value="1">
		<p class="description">
			<?php esc_html_e( 'Check if you want to export text without tags and links to the mediafiles', 'smart-eps' ); ?>
		</p>
		<?php
	}

	/**
	 * Выводит чекбокс, который позволяет убрать из содержимого постов шорткоды
	 *
	 * @param $args array
	 */
	public function export_strip_shotcodes_posts_s( $args ) {
		// get the value of the setting we've registered with register_setting()
		$options = $this->get_eps_options();
		// output the field
		?>
		<input type="checkbox" id="<?php echo esc_attr( $args['label_for'] ); ?>" name="<?php echo SMART_EPS_OPTIONS; ?>[<?php echo esc_attr( $args['label_for'] ); ?>]" <?php echo isset( $options[ $args['label_for'] ] ) ? ( checked( $options[ $args['label_for'] ], '1', false ) ) : ( '' ); ?> value="1">
		<p class="description">
			<?php esc_html_e( 'Check if you want to export text without shotcodes', 'smart-eps' ); ?>
		</p>
		<?php
	}
}
