<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Base class for export.
 *
 * @author Alexander Vitkalov <nechin.va@gmail.com>
 * @copyright (c) 29.04.2019, Vitkalov
 * @version 1.0
 */
class Export_Base {
	/**
	 * @var string
	 */
	public $format = '';

	/**
	 * @var string
	 */
	public $mime_type = '';

	/**
	 * @var string
	 */
	public $title = '';

	/**
	 * @var string
	 */
	public $filename = '';

	/**
	 * Export_Base constructor.
	 *
	 * @param $title
	 * @param $format
	 * @param $mime_type
	 */
	public function __construct( $title, $format, $mime_type ) {
		if ( '' !== $format ) {
			$this->format = $format;
		}

		if ( '' !== $mime_type ) {
			$this->mime_type = $mime_type;
		}

		if ( '' == $title ) {
			$this->title = 'Export ' . $this->format;
		} else {
			$this->title = $title;
		}
	}

	/**
	 * Check if parameter is set in POST
	 *
	 * @param $name string
	 *
	 * @return bool
	 */
	public function is_p( $name ) {
		return isset( $_POST[ SMART_EPS_OPTIONS ][ SMART_EPS_SLUG . '_' . $name ] )
		       && $_POST[ SMART_EPS_OPTIONS ][ SMART_EPS_SLUG . '_' . $name ];
	}

	/**
	 * Get POST parameter value by name
	 *
	 * @param $name string
	 * @param $default mixed
	 *
	 * @return bool|null
	 */
	public function get_p( $name, $default=null ) {
		if ( isset( $_POST[ SMART_EPS_OPTIONS ][ SMART_EPS_SLUG . '_' . $name ] ) ) {
			return $_POST[ SMART_EPS_OPTIONS ][ SMART_EPS_SLUG . '_' . $name ];
		}

		return $default;
	}

	/**
	 * Return parameters array for function get_posts()
	 *
	 * @return array
	 */
	private function get_args() {
		if ( isset( $_POST[ SMART_EPS_OPTIONS ] ) ) {
			$category = $this->get_p( 'category', 0 );

			$date_query = [];
			if ( $this->get_p( 'start_date' ) || $this->get_p( 'end_date' ) ) {
				$date_query = [
					'before'    => $this->get_p( 'end_date' ),
					'after'     => $this->get_p( 'start_date' ),
					'inclusive' => true,
				];
			}

			return [
				'posts_per_page' => $this->get_p( 'count', -1 ),
				'post_type'      => $this->get_p( 'type', 'post' ),
				'date_query'     => $date_query,
				'category'       => $category,
			];
		}

		return [];
	}

	/**
	 * Return posts by parameters
	 *
	 * @return array|int[]|WP_Post[]
	 */
	public function get_posts() {
		$args = $this->get_args();

		if ( ! empty( $args ) ) {
			return get_posts( $args );
		}

		return [];
	}

	/**
	 * Strip html tags if option is set otherwise strip tags body, html, head from html code
	 * Strip shortcode if option is set
	 *
	 * @param $html string
	 *
	 * @return string
	 */
	public function clean_content( $html ) {
		// Strip tags
		if ( $this->is_p( 'strip_tags' ) ) {
			$html = strip_tags( $html );

			if ( 'txt' != $this->format ) {
				$html = str_replace( [ "\n\r", "\r\n", "\t" ], [ '<br />', '<br />', '' ], $html );
			}
		} else {
			// Strip base tags
			$html = preg_replace( '/<!doctype[^>]*>|<[\/]?body[^>]*>|<[\/]?html[^>]*>|<[\/]?head[^>]*>/', '', $html );
		}

		// Strip shortcodes
		if ( $this->is_p( 'strip_shotcodes' ) ) {
			$html = strip_shortcodes( $html );
			$html = preg_replace( '~\[[^\]]+\]~', '', $html );
		}

		return $html;
	}

	/**
	 * Change width attribute value for img tag in content if value greater then 700
	 *
	 * @param $html string
	 * @param $max_width int
	 *
	 * @return string
	 */
	public function change_img_width( $html, $max_width = 700 ) {
		preg_match_all( '/< *img[^>]*>/i', $html, $matches );

		if ( ! empty( $matches[0] ) ) {
			foreach ( $matches[0] as $img_match ) {
				preg_match( '/[\s]+width *= *["\']?([^"\']*)/i', $img_match, $w_matches );
				if ( ! empty( $w_matches[1] ) && $w_matches[1] > $max_width ) {
					preg_match( '/[\s]+height *= *["\']?([^"\']*)/i', $img_match, $h_matches );
					if ( ! empty( $h_matches[1] ) ) {
						$width     = trim( $w_matches[1] );
						$height    = round( $max_width / $width * trim( $h_matches[1] ) );
						$new_match = str_replace( ' width="' . $w_matches[1] . '"', ' width="' . $max_width . '"', $img_match );
						$new_match = str_replace( ' height="' . $h_matches[1] . '"', ' height="' . $height . '"', $new_match );
						$html      = str_replace( $img_match, $new_match, $html );
					}
				}
			}
		}

		return $html;
	}

	/**
	 * Return new line symbol
	 *
	 * @return string
	 */
	private function get_nl() {
		if ( 'txt' == $this->format ) {
			return "\n";
		} else {
			return '<br />';
		}
	}

	/**
	 * Get divider string for the export of the specified type
	 *
	 * @return string
	 */
	public function get_divider() {
		$divider = '';

		if ( $this->is_p( 'posts_divider' ) ) {
			$divider = $this->get_nl();

			switch ( $this->get_p( 'posts_divider' ) ) {
				// Solid line (______)
				case 2:
					$divider .= str_repeat( '_', 81 );
					break;
				// Dashed line (-----)
				case 3:
					$divider .= str_repeat( '-', 135 );
					break;
				// Dotted line (.....)
				case 4:
					$divider .= str_repeat( '.', 148 );
					break;
			}

			$divider .= $this->get_nl();
		}

		return $divider;
	}

}
