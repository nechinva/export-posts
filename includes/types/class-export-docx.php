<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Export in MS doc format.
 *
 * @author Alexander Vitkalov <nechin.va@gmail.com>
 * @copyright (c) 13.08.2019, Vitkalov
 * @version 1.0
 */
class Export_Docx extends Export_Base {
	/**
	 * @var string
	 */
	private $temp_dir = '';

	/**
	 * Export_Doc constructor.
	 *
	 * @param $title string
	 */
	public function __construct( $title ) {
		parent::__construct(
			$title,
			'docx',
			'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
		);

		$this->filename = "{$this->title}.{$this->format}";
		$this->temp_dir = SMART_EPS_DIR_LIBS . 'phpword/temp';

		require_once SMART_EPS_DIR_LIBS . 'phpword/vendor/autoload.php';
	}

	/**
	 * Export posts in doc format
	 *
	 * @see https://github.com/PHPOffice/PHPWord
	 * @return string
	 */
	public function export() {
		$html = '';

		$posts = $this->get_posts();

		if ( ! empty( $posts ) ) {
			foreach ( $posts as $key => $post ) {
				$html .= $post->post_title;
				$html .= '<br />';
				$html .= $this->clean_content( $post->post_content );

				if ( $key <= count( $posts ) ) {
					$html .= $this->get_divider();
				}
			}

			$html = $this->change_img_width( $html, 600 );
			$html = str_replace( '&nbsp;', '', $html );
			$html = str_replace( '&', '', $html );

			// Creating the new document...
			$php_word = new \PhpOffice\PhpWord\PhpWord();

			$section = $php_word->addSection();
			\PhpOffice\PhpWord\Shared\Html::addHtml( $section, $html, false, false );

			/* Set HTTP headers */
			header( 'Content-Description: File Transfer' );
			header( 'Content-Disposition: attachment; filename=' . sanitize_file_name( $this->filename ) );
			header( "Content-Type: $this->mime_type;" );
			header( 'Content-Transfer-Encoding: binary' );
			header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
			header( 'Expires: 0' );

			// Create temp directory
			if (!is_dir($this->temp_dir)) {
				mkdir($this->temp_dir, '775');
			}

			// Saving the document as OOXML file...
			$writer = \PhpOffice\PhpWord\IOFactory::createWriter( $php_word, 'Word2007' );
			$writer->save( $this->temp_dir . '/' . $this->filename );

			if ( file_exists( $this->temp_dir . '/' . $this->filename ) ) {
				$content = file_get_contents( $this->temp_dir . '/' . $this->filename );
				unlink( $this->temp_dir . '/' . $this->filename );

				return $content;
			}
		}

		return '';
	}

}
