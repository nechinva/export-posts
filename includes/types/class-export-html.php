<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Export in html format.
 *
 * @author Alexander Vitkalov <nechin.va@gmail.com>
 * @copyright (c) 29.04.2019, Vitkalov
 * @version 1.0
 */
class Export_Html extends Export_Base {

	/**
	 * @var string
	 */
	private $start_html = '<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>{TITLE}</title>
  <meta name="description" content="{TITLE}">
  <meta name="author" content="Smart Export Posts">
</head>
<body>';

	/**
	 * @var string
	 */
	private $end_html = '</body>
</html>';

	/**
	 * Export_Html constructor.
	 *
	 * @param $title string
	 */
	public function __construct( $title ) {
		parent::__construct( $title, 'html', 'text/html' );

		$this->start_html = str_replace( '{TITLE}', $this->title, $this->start_html );
		$this->filename   = "{$this->title}.{$this->format}";
	}

	/**
	 * Export posts in html format
	 *
	 * @return string
	 */
	public function export() {
		$html = '';

		$posts = $this->get_posts();

		if ( ! empty( $posts ) ) {
			foreach ( $posts as $key => $post ) {
				$html .= $post->post_title;
				$html .= '<br>';
				$html .= $this->clean_content( $post->post_content );

				if ( $key <= count( $posts ) ) {
					$html .= $this->get_divider();
				}
			}

			/* Set HTTP headers */
			header( 'Content-Disposition: attachment; filename=' . sanitize_file_name( $this->filename ) );
			header( "Content-Type: $this->mime_type; charset=" . get_bloginfo( 'charset' ) );
		}

		return $this->start_html . $html . $this->end_html;
	}

}
