<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * Export in pdf format.
 *
 * @author Alexander Vitkalov <nechin.va@gmail.com>
 * @copyright (c) 29.04.2019, Vitkalov
 * @version 1.0
 */
class Export_Pdf extends Export_Base {
	/**
	 * @var string
	 */
	private $start_html = '<!doctype html>
<html lang="en">
<head>
  <title>{TITLE}</title>
  <meta charset="utf-8">
  <meta name="description" content="{TITLE}">
  <meta name="author" content="Smart Export Posts">
</head>
<body>
<div style="width: 700px;overflow: hidden;">';

	/**
	 * @var string
	 */
	private $end_html = '</div></body>
</html>';

	/**
	 * Export_Pdf constructor.
	 *
	 * @param $title string
	 */
	public function __construct( $title ) {
		parent::__construct( $title, 'pdf', 'application/pdf' );

		$this->start_html = str_replace( '{TITLE}', $this->title, $this->start_html );
		$this->filename   = "{$this->title}.{$this->format}";

		require_once SMART_EPS_DIR_LIBS . 'dompdf/autoload.inc.php';
	}

	/**
	 * Export posts in pdf format
	 *
	 * @see https://github.com/dompdf/dompdf
	 * @return string
	 */
	public function export() {
		$html = '';

		$posts = $this->get_posts();

		if ( ! empty( $posts ) ) {
			foreach ( $posts as $key => $post ) {
				$html .= $post->post_title;
				$html .= '<br>';
				$html .= $this->clean_content( $post->post_content );

				if ( $key <= count( $posts ) ) {
					$html .= $this->get_divider();
				}
			}

			// Set options during dompdf instantiation
			$options = new Options();
			$options->set( 'defaultFont', 'times' );
			$options->set( 'isRemoteEnabled', true );
			$options->set( 'defaultPaperSize', 'a4' );

			$html = $this->change_img_width( $html );

			// instantiate and use the dompdf class
			$dompdf = new Dompdf( $options );
			$dompdf->loadHtml( $this->start_html . $html . $this->end_html );

			// Render the HTML as PDF
			$dompdf->render();

			/* Set HTTP headers */
			header( 'Content-Disposition: attachment; filename=' . sanitize_file_name( $this->filename ) );
			header( "Content-Type: $this->mime_type; charset=" . get_bloginfo( 'charset' ) );

			// Return the generated PDF as a string
			return $dompdf->output();
		}

		return '';
	}

}
