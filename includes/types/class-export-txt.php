<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Export in text format.
 *
 * @author Alexander Vitkalov <nechin.va@gmail.com>
 * @copyright (c) 29.04.2019, Vitkalov
 * @version 1.0
 */
class Export_Txt extends Export_Base {

	/**
	 * Export_Text constructor.
	 *
	 * @param $title string
	 */
	public function __construct( $title ) {
		parent::__construct( $title, 'txt', 'plain/text' );

		$this->filename = "{$this->title}.{$this->format}";
	}

	/**
	 * Export posts in text format
	 *
	 * @return string
	 */
	public function export() {
		$text = '';

		$posts = $this->get_posts();

		if ( ! empty( $posts ) ) {
			foreach ( $posts as $key => $post ) {
				$text .= $post->post_title;
				$text .= "\n";

				$content = $this->clean_content( $post->post_content );
				for ( $i = 0; $i < 10; $i ++ ) {
					$content = str_replace( "\n\n", "\n", $content );
				}

				$text .= $content;

				if ( $key <= count( $posts ) ) {
					$text .= $this->get_divider();
				}
			}

			/* Set HTTP headers */
			header( 'Content-Disposition: attachment; filename=' . sanitize_file_name( $this->filename ) );
			header( "Content-Type: $this->mime_type; charset=" . get_bloginfo( 'charset' ) );
		}

		return $text;
	}

}
