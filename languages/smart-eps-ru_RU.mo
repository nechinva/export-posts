��    #      4  /   L           	       2     I   I     �     �     �     �     �     �     �     �  	          
        %     1  
   ?     J     X     s  $   �     �     �  :   �          *  $   A     f     o     ~  
   �     �  
   �  6  �     �     �  e   	  �   o  '   �  *   	  "   J	     m	     �	     �	     �	  #   �	     �	     
      
  #   >
  %   b
     �
  %   �
  @   �
  >     S   D  !   �  $   �  r   �  ,   R  2     9   �     �  %   �      %     F     b     �                                    	              
                    "                                             #                                      !    All Category Check if you want to export text without shotcodes Check if you want to export text without tags and links to the mediafiles Dashed line (-----) Default Settings Dotted line (.....) End date Export Export Error Export format Export settings File name New line No divider Posts count Posts divider Posts type Save Settings Set a start date for posts Set an end date for posts Set divider for posts in export file Set export settings Set posts type Set template name for export file ({TYPE} - export format) Set the category of posts Set the count of posts Set the format of the resulting file Settings Settings saved Solid line (______) Start date Strip shotcodes Strip tags Project-Id-Version: Smart Export Posts
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-06-01 09:06+0300
PO-Revision-Date: 2020-01-30 11:48+0300
Last-Translator: 
Language-Team: vitkalov
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.8.7
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
 Все Категория Отметьте, если в экспорте вам нужен текст без шорткодов Отметьте, если в экспорте вам нужен текст без тегов и ссылок на медиафайлы Пунктирная линия (-----) Настройки по-умолчанию Линия из точек (.....) Конечная дата Экспорт Ошибка экспорта Формат экспорта Настройки экспорта Имя файла Новая строка Без разделителя Количество записей Разделитель записей Тип записей Сохранить настройки Задайте начальную дату для записей Задайте конечную дату для записей Задайте разделитель записей в файле экспорта Задайте настройки Задайте тип записей Задайте шаблон имени для файла экспорта ({TYPE} - формат экспорта) Задайте рубрику записей Задайте количество записей Задайте формат итогового файла Настройки Настройки сохранены Прямая линия (______) Начальная дата Убрать шорткоды Убрать теги 