<?php
/**
 * Plugin Name: Smart Export Posts
 * Description: Smart Export Posts - A plugin that allows you to export posts and pages to one of the selected format (html, pdf, doc, txt) using various settings
 * Version: 0.7.0
 * Author: Alexander Vitkalov
 * Author URI: http://vitkalov.ru
 * License: GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: smart-eps
 * Domain Path: /languages
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'Smart_Eps_Base' ) ) {
	define( 'SMART_EPS_PATH', __FILE__ );
	define( 'SMART_EPS_DIR', dirname( __FILE__ ) . '/' );
	define( 'SMART_EPS_URL', plugins_url( null, __FILE__ ) );
	define( 'SMART_EPS_PLUGIN_BASE', plugin_basename( dirname( __FILE__ ) ) );

	define( 'SMART_EPS_DIR_INCLUDES', SMART_EPS_DIR . 'includes/' );
	define( 'SMART_EPS_DIR_TYPES', SMART_EPS_DIR . 'includes/types/' );
	define( 'SMART_EPS_DIR_TEMPLATES', SMART_EPS_DIR . 'templates/' );
	define( 'SMART_EPS_DIR_LIBS', SMART_EPS_DIR . 'libs/' );

	define( 'SMART_EPS_SLUG', 'smart_eps' );
	define( 'SMART_EPS_OPTIONS', 'smart_eps_options' );
	define( 'SMART_EPS_OPTION_GROUP', 'smart-eps' );

	// Language: ru_RU, en_EN
	if ( ! defined( 'LANG_TYPE' ) ) {
		define( 'LANG_TYPE', 'ru_RU' );
	}

	require_once SMART_EPS_DIR_INCLUDES . 'class-smart-eps-base.php';
}

try {
	$smart_eps_base = Smart_Eps_Base::instance();
	$smart_eps_base->run();
}
catch ( Exception $e ) {
	exit( $e->getMessage() );
}

/**
 * Get the instance
 *
 * @return Smart_Eps_Base|null
 */
function smart_eps_instance() {
	return Smart_Eps_Base::instance();
}
